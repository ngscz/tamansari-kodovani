// Windows 8 mobile Responsive utility
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
  var msViewportStyle = document.createElement('style')
  msViewportStyle.appendChild(
    document.createTextNode(
      '@-ms-viewport{width:auto!important}'
    )
  )
  document.querySelector('head').appendChild(msViewportStyle)
}

// Jquery functions
jQuery(document).ready(function () {
  // LazyLoading
  jQuery("img").unveil(500, function () {
    jQuery(this).on('load', function () {
      jQuery(this).removeAttr("data-src");
    });
  });
  jQuery("#nav-opener").click(function(){
    jQuery(this).toggleClass("is-active");
    jQuery("#nav-container").toggleClass("is-active");
  });
  //jQuery("#nav-container").doubleTapToGo();

  //top slider
  jQuery("#intro-slider").on('init', function(event, slick){
	$(this).find('[data-slick-index="0"]').find('.intro__slide').addClass('in');
  }).on('afterChange', function(event, slick, currentSlide){
	$(this).find('.slick-slide:not(".slick-active") .intro__slide').removeClass('in');
  }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
	$(this).find('[data-slick-index="'+nextSlide+'"]').find('.intro__slide').addClass('in');
  }).slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 750,
    fade: true,
    cssEase: 'ease',
    swipe: false,
	pauseOnHover: false,
	pauseOnFocus: false
  });

  jQuery('.continue-arrow').click(function () {
      var targetElement = jQuery(jQuery(this).attr('href'));
      jQuery('html, body').animate({
          scrollTop: targetElement.offset().top
      }, 1000);

      return this;
  });

	var $elements = $('#top, #place, #contact, #voucher');
	$elements.waypoint({
		handler: function(direction) {
			if(direction === 'down')
			{
				changeMainNavCurrentItemClass(this.element.id);
				if(this.element.id == 'voucher')
				{
					hpAnimation.runIn();
				}
			}
		},
		offset: '50%'
	});
	$elements.waypoint({
		handler: function(direction) {
			if(direction === 'up')
			{
				changeMainNavCurrentItemClass(this.element.id);
			}
		},
		offset: function() {
		  return -this.element.clientHeight
		}
	});
//	$('#voucher').waypoint({
//		handler: function(direction)
//		offset: '50%'
//	});

    function changeMainNavCurrentItemClass(pageName) {
        jQuery("#waypoints a").removeClass("is-active");
        jQuery("#waypoints a[href='#" + pageName + "']").addClass("is-active");
    }
    jQuery('#waypoints a').click(function (event) {
        event.preventDefault();
        var targetElement = jQuery(jQuery(this).attr('href'));
        jQuery('html, body').animate({
            scrollTop: targetElement.offset().top
        }, 1000);
    });

	hpAnimation.init();

});

var hpAnimationObject = function(){
    var self = this;

	self.init = function(){

		self.duration = 3;
		self.container = $('.flowers');
		self.flower1 = $('.span-1-flower', self.container);
		self.flower2 = $('.span-2-flower', self.container);
		self.flower3 = $('.span-3-flower', self.container);
		self.flower4 = $('.span-4-flower', self.container);
		self.text1 = $('.span-1', self.container);
		self.text2 = $('.span-2', self.container);
		self.text3 = $('.span-3', self.container);
		self.text4 = $('.span-4', self.container);
		self.flowers = [self.flower1, self.flower2, self.flower3, self.flower4];
		self.texts = [self.text1, self.text2,self.text3, self.text4];



		self.tl = new TimelineLite({paused: true});
		//self.tl.fromTo(self.container, self.duration, {autoAlpha: 0, scale: 0.5}, {autoAlpha: 1, scale: 1}, 0);

		self.tl.fromTo(self.flower1, self.duration, {autoAlpha: 1, scale: 0.5, y: '30px', x: '200px'}, {autoAlpha: 1, scale: 1, y: 0, x: 0}, 0);
		self.tl.fromTo(self.flower2, self.duration, {autoAlpha: 1, scale: 0.5, y: '-90px', x: '230px'}, {autoAlpha: 1, scale: 1, y: 0, x: 0}, 0.5);
		self.tl.fromTo(self.flower3, self.duration, {autoAlpha: 1, scale: 0.5, y: '90px', x: '-230px'}, {autoAlpha: 1, scale: 1, y: 0, x: 0}, 0);
		self.tl.fromTo(self.flower4, self.duration, {autoAlpha: 1, scale: 0.5, y: '-90px', x: '-200px'}, {autoAlpha: 1, scale: 1, y: 0, x: 0}, 0.5);
		self.tl.staggerFromTo(self.texts, 1, {delay: self.duration*2, autoAlpha: 0}, {autoAlpha: 1}, 0.5);

	}

	self.runIn = function(){
		self.tl.progress(0).play();
	};
	self.runOut = function(){
		self.tl.progress(1).reverse();
	};
};
var hpAnimation = new hpAnimationObject();

jQuery(document).scroll(function() {
  if (jQuery(document).scrollTop() >= 200) {
    jQuery('.header').addClass("scrolled");
  } else {
    jQuery('.header').removeClass("scrolled");
  }
  if (jQuery(document).scrollTop() >= 250) {
    setTimeout(
    function()
    {
      jQuery('.header').css("top", "0").css("opacity", "1");
    }, 500);
  } else {
    jQuery('.header').removeAttr("style");
  }
});
