# Tamansari #

Statický web

## Ostatní nastavení ##

Testovací verze:

http://tamansari.livepreview.cz (pokud nefunguje přímo, tak bude možná potřeba jít na URL /build)

Ostrá verze:

https://www.tamansari.cz

## Deployment ##

Automatický deployment nahrává větev **test** na testovací verzi a větev **master** na ostrou verzi.


