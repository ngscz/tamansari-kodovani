var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var newer = require('gulp-newer');
var imagemin = require('gulp-imagemin');
var spritesmith = require('gulp.spritesmith');
var bulkSass = require('organizze-gulp-sass-bulk-import');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var notify = require("gulp-notify");
var plumber = require('gulp-plumber');

var onError = function (err) {
  notify({
  	title: 'Gulp Task Error',
  	message: 'Check the console.',
		sound: 'Sosumi'
  }).write(err);

  console.log(err.toString());

  this.emit('end');
}


// Compile SASS files
gulp.task('sass', function () {
	return gulp.src('./src/scss/style.scss')
	.pipe(bulkSass())
	.pipe(sourcemaps.init())
	.pipe(plumber({ errorHandle: onError }))
	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
	.on('error', onError)
	.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10', 'Android >= 4.1', 'Safari >= 7', 'iOS >= 7']}))
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('./build'))
	.pipe(browserSync.stream())
	.pipe(notify({
  	title   : 'SASS Task Complete',
    message : 'CSS have been compiled'
  }));
});
// Build JavaScript files
gulp.task('scripts', function () {
	gulp.src([
    'src/js/jquery/*.js',
		'src/js/vendor/*.js',
    'src/js/bootstrap/util.js',
    'src/js/bootstrap/index.js',
		//'src/js/bootstrap/alert.js',
		//'src/js/bootstrap/button.js',
		//'src/js/bootstrap/carousel.js',
		//'src/js/bootstrap/collapse.js',
		//'src/js/bootstrap/dropdown.js',
		'src/js/bootstrap/modal.js',
		//'src/js/bootstrap/scrollspy.js',
		//'src/js/bootstrap/tab.js',
		//'src/js/bootstrap/tooltip.js',
		//'src/js/bootstrap/popover.js',
		'src/js/custom/*.js'
	])
	.pipe(sourcemaps.init({loadMaps: true}))
	.pipe(plumber({ errorHandle: onError }))
  .pipe(concat('ngs-functions.min.js'))
  .pipe(uglify())
	.on('error', onError)
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('build/js'))
  .pipe(browserSync.stream())
	.pipe(notify({
  	title   : 'Javascript Task Complete',
    message : 'JS have been compiled'
  }));
});
// Minify any new images
gulp.task('images', function() {
  // Add the newer pipe to pass through newer images only
  return gulp.src(['!src/images/ico/*', 'src/images/**/*'])
    .pipe(newer('build/images'))
    .pipe(imagemin())
    .pipe(gulp.dest('build/images'))
    .pipe(browserSync.stream());
});
// Do sprite
gulp.task('sprite', function () {
  var spriteData = gulp.src('src/images/ico/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
		imgPath: 'images/bg/sprite.png',
		retinaImgName: 'sprite@2x.png',
		retinaSrcFilter: 'src/images/ico/*@2x.png',
		retinaImgPath: 'images/bg/sprite@2x.png',
    cssName: '_sprites.scss',
		cssTemplate: './spritesmith-retina-mixins.template.mustache',
		padding: 10
  }));
  spriteData.img.pipe(gulp.dest('src/images/bg/'));
	spriteData.css.pipe(gulp.dest('src/scss/modules/'));
});
// BrowserSync settings
gulp.task('browserSync', function(){
	browserSync.init({
    server: {
      baseDir: "./build"
    },
		notify: {
      styles: {
        top: 'auto',
        bottom: '0',
        margin: '0px',
        padding: '5px',
        position: 'fixed',
        fontSize: '16px',
        zIndex: '9999',
        borderRadius: '5px 0px 0px',
        color: 'white',
        textAlign: 'center',
        display: 'block',
        backgroundColor: 'rgba(60, 197, 31, 0.5)'
      }
    },
    files: ['build/*.css', 'build/*.html', 'build/js/*.js']
  })
});

// Watch files for change
gulp.task('watch', function() {
  gulp.watch('src/scss/**/*.scss', ['sass']);
	gulp.watch('src/js/**/*.js', ['scripts']);
	gulp.watch(['src/images/bg/**/*', 'src/images/content/**/*'], ['images']);
});
// Tasks
gulp.task('default', ['watch', 'browserSync']);
